package com.ldim.app.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.security.core.Authentication;

import java.io.IOException;

/**
 * Created by ldim on 06.06.2017.
 */
public interface TokenService {
    String token(Authentication authentication) throws JsonProcessingException;
    Authentication authentication(String token) throws IOException;
}
