package com.ldim.app.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.Date;

/**
 * Created by ldim on 06.06.2017.
 */
public class TokenServiceImpl implements TokenService {
    private final String secret;
    private final long ttl;

    public TokenServiceImpl(String secret, long ttl) {
        this.secret = secret;
        this.ttl = ttl;
    }

    @Override
    public String token(Authentication authentication) throws JsonProcessingException {
        String subject = new ObjectMapper().writeValueAsString(new StoreJwtSubject(authentication));

        return Jwts.builder().setSubject(subject).signWith(SignatureAlgorithm.HS512, secret)
                .setExpiration(new Date(System.currentTimeMillis() + ttl)).compact();
    }

    @Override
    public Authentication authentication(String token) throws IOException {
        Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(secret)).parseClaimsJws(token)
                .getBody();

        StoreJwtSubject subject = new ObjectMapper().readValue(claims.getSubject(), StoreJwtSubject.class);

        PreAuthenticatedAuthenticationToken authentication = new PreAuthenticatedAuthenticationToken(
                new DomainUser(subject.getUsername(), subject.getStore()), null,
                AuthorityUtils.commaSeparatedStringToAuthorityList(subject.getRole()));

        authentication.setDetails(token);

        return authentication;
    }
}
