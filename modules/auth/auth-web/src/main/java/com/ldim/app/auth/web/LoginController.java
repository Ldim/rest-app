package com.ldim.app.auth.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ldim.app.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ldim on 06.06.2017.
 */
@RestController
public class LoginController {
    @Autowired
    public TokenService tokenService;

    @RequestMapping("/login")
    public String login() throws JsonProcessingException {
        return tokenService.token(null);
    }
}
