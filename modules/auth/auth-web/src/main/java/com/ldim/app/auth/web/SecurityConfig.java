package com.ldim.app.auth.web;

import com.ldim.app.security.TokenService;
import com.ldim.app.security.TokenServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ldim on 06.06.2017.
 */
@Configuration
public class SecurityConfig {
    @Bean
    public TokenService tokenService() {
        return new TokenServiceImpl("secret", 5000);
    }
}
